/**
 * Copyright (c) 2014 Axa Holding Belgium, SA. All rights reserved.
 * This software is the confidential and proprietary information of the AXA Group.
 */
package chabernac.angularjs.restservices.model;

public class User {
    private String name;
    private String email;

    public String getName() {
        return name;
    }

    public User setName( String aName ) {
        name = aName;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public User setEmail( String aEmail ) {
        email = aEmail;
        return this;
    }

}
