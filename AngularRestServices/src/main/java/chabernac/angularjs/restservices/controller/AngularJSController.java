package chabernac.angularjs.restservices.controller;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import chabernac.angularjs.restservices.model.User;

@Controller
@RequestMapping( "/angular" )
public class AngularJSController {
    private final Map<String, User> users = new HashMap<>();

    @RequestMapping( value = "/user", method = RequestMethod.GET, produces = "application/json" )
    @ResponseBody
    public User getUserWithParam( @RequestParam( value = "name" ) String aUserName ) {
        return users.get( aUserName );
    }

    @RequestMapping( value = "/user/example", method = RequestMethod.GET, produces = "application/json" )
    @ResponseBody
    public User getExample() {
        return new User().setName( "Guy" ).setEmail( "guy.chauliac@gmail.com" );
    }

    @RequestMapping( value = "/user/{aUserName}", method = RequestMethod.GET, produces = "application/json" )
    @ResponseBody
    public User getUserWithPathVariable( @PathVariable String aUserName ) {
        return users.get( aUserName );
    }

    @RequestMapping( value = "/user/{aUserName}", method = RequestMethod.DELETE, produces = "application/json" )
    @ResponseBody
    public User deleteUser( @PathVariable String aUserName ) {
        return users.remove( aUserName );
    }

    @RequestMapping( value = "/user/list", method = RequestMethod.GET, produces = "application/json" )
    @ResponseBody
    public Collection<User> getUsers() {
        return users.values();
    }

    @RequestMapping( value = "/user", method = RequestMethod.POST, produces = "application/json", consumes = "application/json" )
    @ResponseBody
    public boolean saveUser( @RequestBody User aUser ) {
        users.put( aUser.getName(), aUser );
        return true;
    }

    @RequestMapping( value = "/user", method = RequestMethod.PUT, produces = "application/json", consumes = "application/json" )
    @ResponseBody
    public boolean saveAllUsers( @RequestBody List<User> aUsers ) {
        for ( User theUser : aUsers ) {
            users.put( theUser.getName(), theUser );
        }
        return true;
    }
}
