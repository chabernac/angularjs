import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Copyright (c) 2014 Axa Holding Belgium, SA. All rights reserved.
 * This software is the confidential and proprietary information of the AXA Group.
 */

@Controller
@RequestMapping("/angular")
public class AngularJSController {
    @RequestMapping("/list", method = method=RequestMethod.GET)
    public String getUser(){
        return "Guy";
    }
}
