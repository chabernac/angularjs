var app = angular.modul('app', [ 'ngResource' ]);
app
		.factory(
				'userservice',
				function($resource) {
					return $resource(
							'http://localhost:8080/AngularRestServices/mvc/angular/user/',
							{},
							{
								userDetail : {
									url : 'http://localhost:8080/AngularRestServices/mvc/angular/user/:userId',
									method : 'GET',
									params : {
										userId : '@userId'
									},
									isArray : false
								},
								allUsers : {
									url : 'http://localhost:8080/AngularRestServices/mvc/angular/user/list/',
									method : 'GET',
									isArray : true
								},
								post : {
									method : 'POST',
								},
								saveAll : {
									method : 'PUT'
								},
								remove : {
									url : 'http://localhost:8080/AngularRestServices/mvc/angular/user/:userId',
									method : 'DELETE',
									params : {
										userId : '@userId'
									},
									isArray : false
								}
							});
				});

app.controller("UserController", function($scope, userservice) {
	$scope.result = {};
	$scope.users = [];

	$scope.userDetail = function(id) {
		userservice.userDetail({
			userId : id
		}, function(response) {
			$scope.user = response;
		});
	};

	$scope.editUser = function(user) {
		$scope.user = user;
	};

	$scope.allUsers = function() {
		userservice.allUsers(function(response) {
			$scope.users = response;
		});
	};

	$scope.save = function() {
		$scope.users.push($scope.user);
		userservice.post($scope.user);
		$scope.user = {};
	};

	$scope.saveAll = function() {
		userservice.saveAll($scope.users);
	};

	$scope.deleteUser = function(userName) {
		for (var i = 0; i < $scope.users.length; i++) {
			if ($scope.users[i].name == userName) {
				$scope.users.splice(i, 1);
			}
		}
		userservice.remove({
			userId : userName
		});
	};
});