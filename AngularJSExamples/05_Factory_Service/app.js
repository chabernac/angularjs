	var app = angular.module("app", []);
app.factory("calculator",function(){
	//instantiate a new service object
	var calculatorService = {};
	calculatorService.multiply = function(arg1, arg2){
		return parseInt(arg1) * parseInt(arg2);
	};
	calculatorService.add = function(arg1, arg2){
		return parseInt(arg1) + parseInt(arg2);
	};
	return calculatorService;
});

app.controller("CalculatorController",function($scope, calculator){
	$scope.multiply = function(){
		$scope.result =  calculator.multiply($scope.input.value1, $scope.input.value2);
	};
	$scope.add = function(){
		$scope.result =  calculator.add($scope.input.value1, $scope.input.value2);
	};
});