

angular.module('docsSimpleDirective', [])
.controller('Controller', ['$scope', function($scope) {
	$scope.customer = {
			name: 'Naomi',
			price: ',',
			address: '1600 Amphitheatre'
	};
}])
.directive('myCustomer', function() {
	return {
		template: 'Name: {{customer.name}} Address: {{customer.address}}'
	};
})
.directive('myFreeText',function($parse){
	return {
		restrict: 'AEC'
			,require: '?ngModel'

				,link: function(scope, elem, attrs){
					if (attrs.ngModel) {
						scope.$watch(attrs.ngModel, function(){
							var value = $parse(attrs.ngModel)(scope);
							value = value.replace(/[^0-9\.]/g,'');
							$parse(attrs.ngModel).assign(scope, value);


							//if(!value.match('/[0-9]+([,][0-9]{1,2}){0,1}/')){
							//if(!value.match('[0-9]+([,][0-9]{1,2}){0,1}')){
//							if(!value.match('/^[+-]?\d+(\.\d+)?$/')){
//								elem.attr('class', 'error');
//							} else {

								var theFloat = parseFloat(value);
								if(theFloat == value){
									elem.attr('class', 'ok');
								} else {
									elem.attr('class', 'error');
//									theFloat.toFixed(2);
//									$parse(attrs.ngModel).assign(scope, theFloat.toString());
								}
//							}
						});
					}
				}  
	} ;
})
.controller('TimeController', ['$scope', function($scope) {
	$scope.format = 'M/d/yy h:mm:ss a';
}])
.directive('myCurrentTime', ['$interval', 'dateFilter', function($interval, dateFilter) {

	function link(scope, element, attrs) {
		var format,
		timeoutId;

		function updateTime() {
			element.text(dateFilter(new Date(), format));
		}

		scope.$watch(attrs.myCurrentTime, function(value) {
			format = value;
			updateTime();
		});

		element.on('$destroy', function() {
			$interval.cancel(timeoutId);
		});

		// start the UI update process; save the timeoutId for canceling
		timeoutId = $interval(function() {
			updateTime(); // update DOM
		}, 1000);
	}

	return {
		link: link
	};
}]);

