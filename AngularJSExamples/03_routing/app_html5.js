var app=angular.module('module',['ngRoute']);

app.config
(
	['$routeProvider', '$locationProvider'
	,function($routeProvider, $locationProvider){
		$routeProvider.when('/:exercise/routing_html5.html',
				{
				templateUrl: 'add.html'
				,controller: 'AddController'
				}
			);
		// enable html5Mode for pushstate ('#'-less URLs)
		$locationProvider.html5Mode(true);
		}
	]
);	


app.controller('TestController', function($scope){
	$scope.test='hopla';
});

app.controller('AddController', function($scope, $routeParams){
	$scope.test='Add';
	console.log('exercise: ' + $routeParams.exercise);
});

app.controller('RemoveController', function($scope){
	$scope.test='Remove';
});

app.controller('ErrorController', function($scope){
	$scope.test='Error';
});






      

		
	