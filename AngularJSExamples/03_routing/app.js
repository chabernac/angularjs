var app=angular.module('module',['ngRoute']);

app.controller('TestController', function($scope){
	$scope.test='hopla';
});

app.controller('AddController', function($scope){
	$scope.test='Add';
});

app.controller('RemoveController', function($scope){
	$scope.test='Remove';
});

app.controller('ErrorController', function($scope){
	$scope.test='Error';
});




app.config
(
	['$routeProvider'
	,function($routeProvider){
		$routeProvider.when('/add',
				{
				templateUrl: 'add.html'
				,controller: 'AddController'
				}
			)
			.when('/remove',
					{
					templateUrl: 'remove.html'
					,controller: 'RemoveController' 
					}
			)
			.otherwise(
				{
				templateUrl: 'error.html'
				,controller: 'ErrorController'
				}
			)
		}
	]
);	

      

		
	