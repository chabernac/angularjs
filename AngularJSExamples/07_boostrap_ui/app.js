var app = angular.module('app', ['ui.bootstrap']);

app.controller("AlertDemoCtrl",
		function AlertDemoCtrl($scope) {
			  $scope.alerts = [
			    { type: 'danger', msg: 'Oh snap! Change a few things up and try submitting again.' },
			    { type: 'success', msg: 'Well done! You successfully read this important alert message.' }
			  ];

			  $scope.addAlert = function() {
			    $scope.alerts.push({msg: 'Another alert!'});
			  };

			  $scope.closeAlert = function(index) {
			    $scope.alerts.splice(index, 1);
			  };
			}		
);

app.controller("DateCtrl",
		function AlertDemoCtrl($scope) {
	
	  $scope.today = function() {
	    $scope.dt = new Date();
	  };
	  $scope.today();

	  $scope.clear = function () {
	    $scope.dt = null;
	  };

	  // Disable weekend selection
	  $scope.disabled = function(date, mode) {
	    return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
	  };

	  $scope.toggleMin = function() {
	    $scope.minDate = $scope.minDate ? null : new Date();
	  };
	  $scope.toggleMin();

	  $scope.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.opened = true;
	  };

	  $scope.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1
	  };

	  $scope.initDate = new Date('2016-15-20');
	  $scope.format = 'dd/MM/yyyy';
	}
);

