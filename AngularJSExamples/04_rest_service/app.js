var app=angular.module('rest',[]);

app.controller('RestController', function($scope, $http){
	$scope.load = function() {
		$http.get('http://rest-service.guides.spring.io/greeting')
		.success(function(data, status, headers, config) {
			$scope.greeting = data;
		})
		.error(function(data, status, headers, config) {
			alert(status);
		});
	};

});



