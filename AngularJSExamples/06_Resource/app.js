var app = angular.module('app', ['ngResource']);
app.factory('service',function($resource){
	return $resource('https://api.github.com/repos/angular/angular.js/issues/:entryId', {},
			{
		detail: {method:'GET', params:{entryId:'@entryId'}, isArray:false},
		loadAll: {method:'GET', isArray:true},
		post: {method:'POST'},
		update: {method:'PUT'},
		remove: {method:'DELETE'}
			}
	); 
});

app.controller("ServiceController",function($scope, service){
	$scope.result = {};
	$scope.loadAll = function(){
		service.loadAll(function(response){
			$scope.result.data = response;
		});
	};

	$scope.detail=function(id){
		service.detail({entryId:id}, function(response){
			$scope.result.detail= response.body;
		});
	};
});